const DiscoNode = require('disconode');
const config = require('config.js');

let disco = new DiscoNode.Disco(config.token);

disco.login().then(function() {
    console.log('Logged in.');
});

disco.registerCommand(config.prefix + 'ping', function(message) {
    message.soft.reply('pong');
}, 'Replies with pong.');

disco.registerCommand(config.prefix + 'say', function(message) {
    message.delete();
    message.channel.send(message.commandFreeContent);
}, 'Repeats the message.');

disco.setHelpCommand(config.prf'help', '**Oneines Help Page**', true);